name := """Kafka"""
organization := "ncinga"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"

libraryDependencies += guice
libraryDependencies += "org.apache.kafka" %% "kafka" % "2.4.1"
libraryDependencies += "log4j" % "log4j" % "1.2.17"